#!/usr/bin/env python3

import json
from collections import OrderedDict

DNN_NAMES = ['ResNet', 'SqueezeNet', 'InceptionV3', 'DenseNet']


class Detector():
    """
    Run image detection and return an OrderedDict of labels
    together with their probabilities.
    """
    def __init__(self, model):
        from imageai.Prediction import ImagePrediction
        self.prediction = ImagePrediction()
        if model == "InceptionV3":
            self.prediction.setModelTypeAsInceptionV3()
            self.prediction.setModelPath("models/inception_v3_weights_tf_dim_ordering_tf_kernels.h5")
        elif model == "ResNet":
            self.prediction.setModelTypeAsResNet()
            self.prediction.setModelPath("models/resnet50_weights_tf_dim_ordering_tf_kernels.h5")
        elif model == "SqueezeNet":
            self.prediction.setModelTypeAsSqueezeNet()
            self.prediction.setModelPath("models/squeezenet_weights_tf_dim_ordering_tf_kernels.h5")
        elif model == "DenseNet":
            self.prediction.setModelTypeAsDenseNet()
            self.prediction.setModelPath("models/DenseNet-BC-121-32.h5")
        else:
            raise ValueError(f"Unknown model: {model}")
        self.prediction.loadModel(prediction_speed="fast")

    def detect(self, image):
        predictions, probabilities = self.prediction.predictImage(image, result_count=5)
        return OrderedDict(zip(predictions, probabilities))

class CustomDetector(Detector):
    """
    Image detection using a custom-trained model.
    """
    def __init__(self, model, model_file, model_json):
        from imageai.Prediction.Custom import CustomImagePrediction
        self.prediction = CustomImagePrediction()
        self.prediction.setModelPath(model_file)
        self.prediction.setJsonPath(model_json)
        with open(model_json, 'rt') as f:
            labels = json.load(f)
            num_labels = len(labels.keys())

        if model:
            if model == "InceptionV3":
                self.prediction.setModelTypeAsInceptionV3()
            elif model == "ResNet":
                self.prediction.setModelTypeAsResNet()
            elif model == "SqueezeNet":
                self.prediction.setModelTypeAsSqueezeNet()
            elif model == "DenseNet":
                self.prediction.setModelTypeAsDenseNet()
            else:
                raise ValueError(f"Unknown model: {model}")
            self.prediction.loadModel(num_objects=num_labels)
        else:
            self.prediction.loadFullModel(num_objects=num_labels)
