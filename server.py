#!/usr/bin/env python3

import time
import json
import argparse
from urllib.parse import urlparse, parse_qs
from http.server import BaseHTTPRequestHandler, HTTPServer

from detector import Detector, CustomDetector, DNN_NAMES

PORT_NUMBER = 8080

detector = None


# This class will handles any incoming request from
# the browser
class RequestHandler(BaseHTTPRequestHandler):

    # Handler for the GET requests
    def do_GET(self):
        if self.command not in ['GET']:
            self.send_error(405, "Method Not Allowed")
            return

        parsed_path = urlparse(self.path)
        path = parsed_path.path
        params = parse_qs(parsed_path.query)

        if path == '/healthcheck':
            self.send_response(200)
            self.send_header('Content-type', 'text/plain')
            self.end_headers()
            self.wfile.write(b'OK')
            return

        if path == '/detect':
            if 'file' not in params:
                self.send_error(400, "Bad Request", "Mandatory parameter 'file' is missing.")
                return

            image = params['file'][0]

            try:
                ts = time.time()
                labels = detector.detect(image)
                ts = time.time() - ts
            except ValueError as e:
                self.send_error(404, "Not Found", f'{image}: {e}')
                return
            response = {
                'file': image,
                'labels': labels,
                'elapsed': ts,
            }

            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps(response, indent=2).encode('utf-8'))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=PORT_NUMBER, help='HTTP Port')
    parser.add_argument('--dnn', type=str, choices=DNN_NAMES,
                        help='Deep Neural Network Type')
    parser.add_argument('--custom-model', type=str, metavar='FILE.h5',
                        help='Custom-trained model file')
    parser.add_argument('--custom-json', type=str, metavar='FILE.json',
                        help='Labels for custom-trained model file')
    args = parser.parse_args()

    if not args.dnn and not args.custom_model:
        parser.error('Either --dnn or --custom-model or both must be set.')

    if args.custom_model and not args.custom_json:
        parser.error('--custom-model must be used together with --custom-json')

    return args


def main():
    global detector

    args = parse_args()

    if args.custom_model:
        print(f'Loading {args.dnn or "model"} from {args.custom_model} ...')
        detector = CustomDetector(args.dnn, args.custom_model, args.custom_json)
    else:
        print(f'Loading {args.dnn} neural network...')
        detector = Detector(args.dnn)

    try:
        # Create a web server and define the handler to manage the
        # incoming request
        server = HTTPServer(('', args.port), RequestHandler)
        print(f'Started http server on port {args.port}')

        # Wait forever for incoming http requests
        server.serve_forever()

    except KeyboardInterrupt:
        print('^C received, shutting down the web server')
        server.socket.close()


if __name__ == '__main__':
    main()
