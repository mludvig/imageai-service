from collections import OrderedDict
import pytest

from detector import Detector


@pytest.mark.parametrize('model', ('InceptionV3', 'ResNet', 'SqueezeNet', 'DenseNet'))
def test_detector(model):
    detector = Detector(model)
    labels = detector.detect('images/orange.jpg')
    print(f'{model}: {labels}')
    assert type(labels) == OrderedDict
    assert 'orange' in labels
    assert labels['orange'] > 80
    assert labels['orange'] <= 100
    assert 'lemon' not in labels or labels['lemon'] < 10
