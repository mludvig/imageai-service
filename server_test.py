# For now the server.py process must be started manually
# before running these tests.

import pytest
import requests


from server import PORT_NUMBER

base_url = f'http://localhost:{PORT_NUMBER}'


def test_healthcheck():
    url = f'{base_url}/healthcheck'
    resp = requests.get(url)
    assert resp.status_code == 200
    assert 'OK' in resp.text


def test_detect_missing_file_param():
    url = f'{base_url}/detect'
    resp = requests.get(url)
    assert resp.status_code == 400


def test_detect_invalid_filename():
    url = f'{base_url}/detect?file=/non-existent.jpg'
    resp = requests.get(url)
    assert resp.status_code == 404


@pytest.mark.parametrize('file,label', [('images/orange.jpg', 'orange')])
def test_detection(file, label):
    url = f'{base_url}/detect?file={file}'
    resp = requests.get(url)
    print(resp.json())
    assert resp.status_code == 200
    assert resp.json()['labels'][label] > 80
